package com.example.administrator.viewpagerapp;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Administrator on 2017/9/8.
 */

public class FragmentFive extends BaseFrameng {

  private TextView mTextView;

  @Override
  public View createView() {
    mTextView = new TextView(mActivity);
    mTextView.setTextColor(Color.RED);
    mTextView.setTextSize(20f);
    return mTextView;
  }

  @Override
  public void load() {
    mTextView.setText("five");
  }

  @Override
  public void onInvisible() {
    System.out.println("FragmentFive.onInvisible");
  }

  @Override
  public void onVisible() {
    System.out.println("FragmentFive.onVisible");
  }
}
