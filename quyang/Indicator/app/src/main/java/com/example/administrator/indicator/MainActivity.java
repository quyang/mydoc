package com.example.administrator.indicator;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnPageChangeListener {

  private ViewPager mViewPager;
  private TraingleIndicator mIndicator;
  private List<String> mList = Arrays.asList("1", "2", "3", "4");

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    changeActionBar();
//
//    mViewPager = (ViewPager) findViewById(R.id.viewpager);
//
//    mIndicator = (TraingleIndicator) findViewById(R.id.indicator);
//    mIndicator.bindIndicator(mViewPager);
//    mIndicator.setOnItemClick();
//
//    mViewPager.addOnPageChangeListener(this);
//
//    mViewPager.setAdapter(new PagerAdapter() {
//      @Override
//      public int getCount() {
//        return 4;
//      }
//
//      @Override
//      public boolean isViewFromObject(View view, Object object) {
//        return view == object;
//      }
//
//      @Override
//      public Object instantiateItem(ViewGroup container, int position) {
//        TextView textView = new TextView(getApplicationContext());
//        textView.setText(mList.get(position));
//        textView.setTextColor(Color.RED);
//        container.addView(textView);
//        return textView;
//      }
//
//      @Override
//      public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView((View) object);
//      }
//    });

  }

  private Toolbar mToolbar;

  private void changeActionBar() {
//    设定状态栏的颜色，当版本大于4.4时起作用
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
      SystemBarTintManager tintManager = new SystemBarTintManager(this);
      tintManager.setStatusBarTintEnabled(true);
      tintManager.setStatusBarTintResource(android.R.color.holo_red_dark);
    }

    mToolbar = (Toolbar) findViewById(R.id.toolbar);
    mToolbar.setTitle("mainTitle");// 标题的文字需在setSupportActionBar之前，不然会无效
    mToolbar.setTitleTextColor(Color.parseColor("#ff0000"));
    setSupportActionBar(mToolbar);
    mToolbar.setNavigationIcon(android.R.drawable.ic_delete);
    mToolbar.setLogo(android.R.drawable.ic_media_play);

  }

  @Override
  public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    mIndicator.scroll(position,positionOffset);
  }

  @Override
  public void onPageSelected(int position) {

  }

  @Override
  public void onPageScrollStateChanged(int state) {

  }
}
