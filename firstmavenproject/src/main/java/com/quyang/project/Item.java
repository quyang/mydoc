package com.quyang.project;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("quyang")
public class Item {

	@ResponseBody
	@RequestMapping(value = "/hh",produces = {"application/json;charset=UTF-8"})
	public String name() {
		return "helloworld";
	}

}
